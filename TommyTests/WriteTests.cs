﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Text;
using Tommy;

namespace TommyTests
{
    [TestClass]
    public class WriteTests
    {
        [TestMethod]
        public void TestArrayConstruct()
        {
            string expectedResult = @"array = [ ""hello world"" ]";

            TomlTable table = new TomlTable
            {
                ["array"] = new TomlArray
                {
                    "hello world"
                }
            };

            StringBuilder sb = new StringBuilder();
            using (StringWriter sw = new StringWriter(sb))
                table.WriteTo(sw);

            string actualResult = sb.ToString();

            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void TestArrayConstruct2()
        {
            string expectedResult = @"array = [ ""hello world"" ]";

            TomlTable table = new TomlTable
            {
                ["array"] = new TomlArray
                {
                    [0] = new TomlString()
                    {
                        Value = "hello world"
                    }
                }
            };

            StringBuilder sb = new StringBuilder();
            using (StringWriter sw = new StringWriter(sb))
            {
                table.WriteTo(sw);
            }

            string actualResult = sb.ToString();

            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void ObjectConstructTest()
        {
            TomlTable node = new TomlTable
            {
                ["hello"] = new TomlTable
                {
                    Comment = "This table is used for Hello, world -commands!",
                    ["key"] = "wew",
                    ["key2"] = "we\\\\w2",
                    ["key3"] = new TomlString
                    {
                        Value = "wew\\\\w2",
                        PreferLiteral = true,
                    },
                    ["test"] = new TomlTable
                    {
                        Comment = "This is another section table!",
                        ["foo"] = new TomlString
                        {
                            CollapseLevel = 1,
                            Value = "Value"
                        },
                        ["bar"] = new TomlInteger
                        {
                            Comment = "How many bars there are to eat",
                            Value = 10
                        }
                    }
                },
                ["inline-test"] = new TomlTable
                {
                    IsInline = true,
                    ["foo"] = 10,
                    ["bar"] = "test",
                    ["baz"] = new TomlTable
                    {
                        ["qux"] = new TomlString
                        {
                            CollapseLevel = 1,
                            Value = "test"
                        }
                    }
                },
                ["value"] = 10.0,
                ["table-test"] = new TomlArray
                {
                    IsTableArray = true,
                    [0] = new TomlTable
                    {
                        ["wew"] = "wew"
                    },
                    [1] = new TomlTable
                    {
                        ["wew"] = "veemo"
                    },
                    [2] = new TomlTable
                    {
                        ["wew"] = "woomy",
                        ["wobbly"] = new TomlTable
                        {
                            ["baz"] = new TomlString
                            {
                                CollapseLevel = 1,
                                Value = "test"
                            }
                        }
                    }
                }
            };

            using (StringWriter sw = new StringWriter())
            {
                node.WriteTo(sw);

                string s = sw.ToString();
                File.WriteAllText("out.toml", s);
            }
        }

        [TestMethod]
        public void ObjectConstructTest2()
        {
            TomlTable node = new TomlTable
            {
                //["something"] = new TomlInteger { Value = 2 },
                ["hello"] = new TomlTable
                {
                    ["key"] = "wew",
                },
                ["world"] = new TomlTable
                {
                    ["key"] = "wew",
                },
                ["greeting"] = new TomlTable
                {
                    ["key"] = new TomlInteger { Value = 1 },
                },
                ["planet"] = new TomlTable
                {
                    ["key"] = "wew",
                }
            };

            using (StringWriter sw = new StringWriter())
            {
                node.WriteTo(sw);
                
                string s = sw.ToString();
                File.WriteAllText("out2.toml", s);
            }
        }

        [TestMethod]
        public void SectionSpacingTest()
        {
            var node = new TomlTable
            {
                ["parent"] = new TomlInteger
                {
                    Value = 10
                },
                ["hello"] = new TomlTable
                {
                    Comment = "hello table",
                    ["key"] = new TomlString
                    {
                        Comment = "hello world",
                        Value = "world"
                    }
                },
                ["world"] = new TomlTable
                {
                    ["key"] = new TomlString
                    {
                        Comment = "hello world",
                        Value = "world"
                    }
                },
                ["universe"] = new TomlTable
                {
                    Comment = "universe table",
                    ["key"] = new TomlString
                    {
                        Comment = "hello universe",
                        Value = "universe"
                    }
                }
            };

            string test = @"parent = 10

# hello table
[hello]
# hello world
key = ""world""

[world]
# hello world
key = ""world""

# universe table
[universe]
# hello universe
key = ""universe""";

            using (var sw = new StringWriter())
            {
                node.WriteTo(sw);

                string s = sw.ToString();

                
                Assert.AreEqual(test, s);
                File.WriteAllText("out.toml", s);
            }
        }

        [TestMethod]
        public void SectionSpacingTest2()
        {
            var node = new TomlTable
            {
                ["hello"] = new TomlTable
                {
                    Comment = "hello table",
                    ["key"] = new TomlString
                    {
                        Comment = "hello world",
                        Value = "world"
                    }
                }
            };

            string test = @"# hello table
[hello]
# hello world
key = ""world""";

            using (var sw = new StringWriter())
            {
                node.WriteTo(sw);

                string s = sw.ToString();
                Assert.AreEqual(test, s);
                File.WriteAllText("out.toml", s);
            }
        }

        [TestMethod]
        public void SectionSpacingTest3()
        {
            var node = new TomlTable
            {
                ["parent"] = new TomlInteger
                {
                    Value = 10
                },
                ["hello"] = new TomlTable
                {
                    Comment = "hello table",
                    ["key"] = new TomlString
                    {
                        Comment = "hello world",
                        Value = "world"
                    }
                },
                ["world"] = new TomlTable
                {
                    ["key"] = new TomlString
                    {
                        Comment = "hello world",
                        Value = "world"
                    }
                }
            };

            string test = @"parent = 10

# hello table
[hello]
# hello world
key = ""world""

[world]
# hello world
key = ""world""";

            using (var sw = new StringWriter())
            {
                node.WriteTo(sw);

                string s = sw.ToString();
                Assert.AreEqual(test, s);
                File.WriteAllText("out.toml", s);
            }
        }
    }
}
